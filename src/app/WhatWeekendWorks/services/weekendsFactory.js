angular.module('WhatWeekendWorks').
factory('weekendsFactory', function($resource, cfg) {
	return $resource(cfg.apiRoot + 'weekends');
});