angular.module('WhatWeekendWorks').
factory('plansFactory', function($resource, cfg) {
	return $resource(cfg.apiRoot + 'plans');
});