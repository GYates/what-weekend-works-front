angular.module('WhatWeekendWorks').
service('messagesService', function() {
	var messages = {};
	messages.errors = [];
	messages.notices = [];

	return {
		addError: function(msg) {
			messages.errors.push(msg);
		},
		setErrors: function(msg) {
			if (typeof (msg) == 'string') {
				messages.errors = [msg];
			} else {
				messages.errors = msg;
			}
		},
		resetErrors: function() {
			messages.errors = [];
		},
		addNotice: function(msg) {
			messages.notices.push(msg);
		},
		setNotices: function(msg) {
			if (typeof (msg) == 'string') {
				messages.notices = [msg];
			} else {
				messages.notices = msg;
			}
		},
		resetNotices: function() {
			messages.notices = [];
		},
		get: function() {
			return messages;
		}
	};
});