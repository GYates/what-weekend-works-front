angular.module('WhatWeekendWorks').
controller('rootCtrl', function($scope, plansFactory) {
	plansFactory.get(function(response) {
		$scope.plans = response.data;
	});
});