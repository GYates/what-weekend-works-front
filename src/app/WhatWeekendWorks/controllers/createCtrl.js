angular.module('WhatWeekendWorks').
controller('createCtrl', function($scope, weekendsFactory, messagesService, plansFactory) {
	$scope.weekends = weekendsFactory.get();
	
	var errorCheck = function() {
		// Setting up, we're resetting if there are any errors
		var error = false;
		$scope.ownerNameError = false;
		$scope.ownerEmailError = false;
		$scope.planNameError = false;
		$scope.planStartError = false;
		messagesService.resetErrors();

		// Error Checking
		if (typeof ($scope.newPlan) == 'undefined') {
			messagesService.addError('You need to fill out the form.');
			return false;
		}

		if (typeof ($scope.newPlan.ownerName) == 'undefined' || $scope.newPlan.ownerName === '') {
			messagesService.addError('You need to provide a name for the owner of this form.');
			$scope.ownerNameError = true;
			error = true;
		}

		if (typeof ($scope.newPlan.ownerEmail) == 'undefined' || $scope.newPlan.ownerEmail === '') {
			messagesService.addError('You need to provide an email for the owner of this form.');
			$scope.ownerEmailError = true;
			error = true;
		}

		if (typeof ($scope.newPlan.name) == 'undefined' || $scope.newPlan.name === '') {
			messagesService.addError('You must provide a name for this plan.');
			$scope.planNameError = true;
			error = true;
		}

		if (typeof ($scope.newPlan.start) == 'undefined' || $scope.newPlan.start === '') {
			messagesService.addError('You must provide a start date for this plan.');
			$scope.planStartError = true;
			error = true;
		}

		return error;
	};

	$scope.addPlan = function() {
		var error = errorCheck();

		if (!error) {
			plansFactory.save($scope.newPlan, function() {
				$scope.newPlan = null;
			});
		}
	};
});