angular.module('WhatWeekendWorks').
config(function($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: '/views/root.tpl.html',
			controller: 'rootCtrl'
		}).

		otherwise({
			redirectTo: '/'
		});
});