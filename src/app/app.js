angular.module('WhatWeekendWorks', ['ngRoute', 'ngResource'])
.config(function($locationProvider) {
	$locationProvider.
		html5Mode(true).
		hashPrefix('!');
});