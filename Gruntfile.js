/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    ngmin: {
      main: {
        files: [
         {
          expand: true,
          cwd: 'src/app',
          src: ['*.js'],
          dest: 'src/app.gen'
         }
        ]
      },
      components: {
        files: [
          {
            expand: true,
            cwd: 'src/app',
            src: [
              '**/*.js'
            ],
            dest: 'src/app.gen'
          }
        ]
      }
    },
    // In this concatenation task is where we set the order of loading the angular files
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true,
        seperator: ';'
      },
      head: {
        src: ['src/assets/components/jquery/jquery.js', 'src/assets/components/angular/angular.js', 'src/assets/components/angular-route/angular-route.js', 'src/assets/components/angular-resource/angular-resource.js'],
        dest: 'src/assets/js/header.js'
      },
      headMin: {
        src: ['src/assets/components/jquery/jquery.min.js', 'src/assets/components/angular/angular.min.js', 'src/assets/components/angular-route/angular-route.min.js', 'src/assets/components/angular-resource/angular-resource.min.js'],
        dest: 'src/assets/js/header.min.js'
      },
      foot: {
        // Set the order that the angular files should be loaded here
        src: [
          'src/app.gen/*.js',
          'src/app.gen/**/*.js'
        ],
        dest: 'src/assets/js/footer.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>',
        preserveComments: false,
        semicolons: true
      },
      foot: {
        src: ['src/assets/js/footer.js'],
        dest: 'src/assets/js/footer.min.js'
      }
    },
    jshint: {
      options: {
        curly: true, // Forces curlies around all statements
        eqeqeq: false, // Forces all equality checks to be typed
        immed: true, // Forces wrapping immediate function calls with parantheses
        latedef: true, // Prohibits use of a variable before it's defined
        newcap: true, // Requires capitilization of constructor functions
        noarg: true, // Prohibits arguments.caller
        sub: true, // Suppresses warnings about forcing dot notation
        undef: true, // Prohibits the use of explicitly undeclared variables
        unused: true, // Lets you know if you've got unused variables
        eqnull: true, // Suppresses warnings about equality checks against null (a == null)
        browser: true, // Defines globals exposed by modern browsers
        lastsemic: true, // Suppreses warnings about missing semicolons if its the last statement of the block
        expr: true, // Suppresses warnings about expressions where you'd expect to see assignments or function calls, needs to be true cause of uglify
        devel: true, // Adds globals for development like console and alert
        jquery: true, // Adds globals for jQuery
        globals: {
          'angular': true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      angular: {
        src: ['src/app/*.js', 'src/app/**/*.js']
      },
      data: {
        src: ['src/assets/data/*/*.json']
      },
      requests: {
        src: ['src/assets/requests/*/*.json']
      }
    },
    scsslint: {
      options: {
        reporterOutput: null,
        config: 'scss-lint.yml',
        force: true
      },
      allFiles: [
        'src/assets/scss/*.scss',
        'src/assets/scss/**/*.scss'
      ]
    },
    sass: {
      dist: {
        options: {
          style: 'compressed',
          banner: '<%= banner %>'
        },
        files: [{
          expand: true,
          cwd: 'src/assets/scss',
          src: ['*.scss'],
          dest: 'src/assets/css',
          ext: '.css'
        }]
      }
    },
    clean: {
      ngMinTmp: {
        src: ['src/app.gen']
      }
    },
    watch: {
      angular: {
        options: {
          cwd: 'src/app'
        },
        files: ['*.js', '**/*.js'],
        tasks: ['angular']
      },
      data: {
        options: {
          cwd: 'src/assets/data'
        },
        files: ['*.json', '**/*.json'],
        tasks: ['jshint:data', 'notify:data']
      },
      requests: {
        options: {
          cwd: 'src/assets/requests'
        },
        files: ['*.json', '**/*.json'],
        tasks: ['jshint:requests', 'notify:requests']
      },
      scss: {
        options: {
          cwd: 'src/assets/scss'
        },
        files: ['*.scss', '**/*.scss'],
        tasks: ['scss']
      },
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile', 'notify:gruntfile']
      }
    },
    notify: {
      everything: {
        options: {
          title: 'Everything Compiled',
          message: 'Grunt default run and everythings compiled!'
        }
      },
      angular: {
        options: {
          title: 'Angular Compiled',
          message: 'Angular successfully compiled!'
        }
      },
      data: {
        options: {
          title: 'Data Linted',
          message: 'Everything is valid JSON'
        }
      },
      requests: {
        options: {
          title: 'Data Linted',
          message: 'Everything is valid JSON'
        }
      },
      scss: {
        options: {
          title: 'SCSS Compiled',
          message: 'SCSS Compiled and Complete'
        }
      },
      gruntfile: {
        options: {
          title: 'Gruntfile Linted',
          message: 'Gruntfile successfully linted!'
        }
      }
    }
  });

  // Tasks to force and unforce
  grunt.registerTask('forceOn', 'set force option on', function() {
    if ( !grunt.option('force')) {
      grunt.config.set('useForce', true);
      grunt.option('force', true);
    }
  });

  grunt.registerTask('forceOff', 'set force option off', function() {
    if (grunt.config.get('useForce')) {
      grunt.option('force', false);
    }
  });


  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-scss-lint');
  grunt.loadNpmTasks('grunt-ngmin');
  // Enable notifications for OSX
  grunt.loadNpmTasks('grunt-notify');

  // Default task.
  grunt.registerTask('default', ['jshint:gruntfile', 'jshint:angular', 'jshint:data', 'ngmin', 'concat', 'uglify', 'scss', 'clean', 'notify:everything', 'watch']);
  grunt.registerTask('angular', ['jshint:angular', 'ngmin', 'concat:foot', 'uglify:foot', 'clean:ngMinTmp', 'notify:angular']);
  grunt.registerTask('scss', ['forceOn', 'scsslint', 'forceOff', 'sass:dist', 'notify:scss']);
};
